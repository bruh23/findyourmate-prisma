const { forwardTo } = require('prisma-binding')

async function category (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function categories (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    category,
    categories
  }