const { forwardTo } = require('prisma-binding')

async function signalement (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function signalements (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    signalement,
    signalements
  }