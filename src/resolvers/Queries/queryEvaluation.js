const { forwardTo } = require('prisma-binding')

async function evaluation (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function evaluations (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    evaluation,
    evaluations
}