const { forwardTo } = require('prisma-binding')

async function typeBan (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function typesBan (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    typeBan,
    // typesBan
}
