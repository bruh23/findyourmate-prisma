const { forwardTo } = require('prisma-binding')

async function badge (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function badges (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    badge,
    badges
}