const { forwardTo } = require('prisma-binding')

async function block (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function blocks (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    block,
    blocks
}
