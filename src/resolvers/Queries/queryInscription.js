const { forwardTo } = require('prisma-binding')

async function inscription (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function inscriptions (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    inscription,
    inscriptions
}
