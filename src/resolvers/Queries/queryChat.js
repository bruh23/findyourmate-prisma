const { forwardTo } = require('prisma-binding')

async function chat (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function chats (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    chat,
    chats
  }