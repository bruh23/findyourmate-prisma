const { forwardTo } = require('prisma-binding')

async function niveaux (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function niveaux_multiple (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    niveaux,
    // niveaux_multiple
}
