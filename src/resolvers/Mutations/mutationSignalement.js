const { forwardTo } = require('prisma-binding')

async function createSignalement (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateSignalement (parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createSignalement,
    updateSignalement
}