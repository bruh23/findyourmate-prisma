const { forwardTo } = require('prisma-binding')

async function createBlock (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateBlock (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createBlock,
    updateBlock
}
