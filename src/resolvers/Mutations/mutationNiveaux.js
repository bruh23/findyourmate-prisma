const { forwardTo } = require('prisma-binding')

async function createNiveaux (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateNiveaux (parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createNiveaux,
    updateNiveaux
}