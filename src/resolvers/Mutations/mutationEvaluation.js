const { forwardTo } = require('prisma-binding')

async function createEvaluation (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateEvaluation (parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createEvaluation,
    updateEvaluation
}