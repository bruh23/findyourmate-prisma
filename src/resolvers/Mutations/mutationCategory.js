const { forwardTo } = require('prisma-binding')

async function createCategory (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateCategory (parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createCategory,
    updateCategory
}