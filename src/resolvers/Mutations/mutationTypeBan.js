const { forwardTo } = require('prisma-binding')

async function createTypeBan (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateTypeBan (parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createTypeBan,
    updateTypeBan
}
