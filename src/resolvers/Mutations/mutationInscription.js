const { forwardTo } = require('prisma-binding')

async function createInscription (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
  createInscription,
}
