const { forwardTo } = require('prisma-binding')

async function createChat (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateChat (parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createChat,
    updateChat
}
