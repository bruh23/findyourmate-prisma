const { forwardTo } = require('prisma-binding')

async function createCertification (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateCertification (parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createCertification,
    updateCertification
}