const { forwardTo } = require('prisma-binding')

async function createBadge (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateBadge (parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createBadge,
    updateBadge
}