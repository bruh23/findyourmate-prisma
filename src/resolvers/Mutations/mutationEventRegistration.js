const { forwardTo } = require('prisma-binding')

async function createEventRegistration (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateEventRegistration (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createEventRegistration,
    updateEventRegistration
}
