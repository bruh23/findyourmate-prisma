const { user, users } = require('./Queries/queryUser');
const { certification, certifications } = require('./Queries/queryCertification');
const { badge, badges } = require('./Queries/queryBadge');
const { typeBan, typesBan } = require('./Queries/queryTypeBan')
const { evaluation, evaluations } = require('./Queries/queryEvaluation');
const { signalement, signalements } = require('./Queries/querySignalement');
const { niveaux, niveaux_multiple } = require('./Queries/queryNiveaux');
const { event, events } = require('./Queries/queryEvent');
const { category, categories } = require('./Queries/queryCategory');
const { chat, chats } = require('./Queries/queryChat');
const { inscription, inscriptions } = require('./Queries/queryInscription')
const { eventRegistration, eventsRegistration } = require('./Queries/queryEventRegistration');
const { block, blocks } = require('./Queries/queryBlock');


const Query = {
    inscription,
    inscriptions,

    block,
    blocks,

    user,
    users,

    evaluation,
    evaluations,

    signalement,
    signalements,

    niveaux,
    // niveaux_multiple,

    certification,
    certifications,

    badge,
    badges,

    typeBan,
    // typesBan,

    event,
    events,

    category,
    categories,

    chat,
    chats,

    eventRegistration,
    // eventsRegistration
}

module.exports = {
    Query
}
