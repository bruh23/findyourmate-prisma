const { createCertification, updateCertification, } = require("./Mutations/mutationCertification");
const { createBadge, updateBadge } = require("./Mutations/mutationBadge");
const { createTypeBan, updateTypeBan } = require("./Mutations/mutationTypeBan");
const { createEvaluation, updateEvaluation } = require("./Mutations/mutationEvaluation");
const { createNiveaux, updateNiveaux } = require("./Mutations/mutationNiveaux");
const { createSignalement, updateSignalement } = require("./Mutations/mutationSignalement");
const { createEvent, updateEvent } = require("./Mutations/mutationEvent");
const { createCategory, updateCategory } = require("./Mutations/mutationCategory");
const { createChat, updateChat } = require("./Mutations/mutationChat");
const { createUser, updateUser } = require("./Mutations/mutationUser");
const { createEventRegistration, updateEventRegistration } = require('./Mutations/mutationEventRegistration');
const { createBlock, updateBlock } = require('./Mutations/mutationBlock');
const { createInscription } = require('./Mutations/mutationInscription');
const { login, signup, me } = require('./auth')

const Mutation = {
  createEvaluation,
  updateEvaluation,

  createInscription,

  createBlock,
  updateBlock,

  createNiveaux,
  updateNiveaux,

  createSignalement,
  updateSignalement,

  createCertification,
  updateCertification,

  createBadge,
  updateBadge,

  createTypeBan,
  updateTypeBan,

  createEvent,
  updateEvent,

  createCategory,
  updateCategory,

  createChat,
  updateChat,

  createUser,
  updateUser,

  createEventRegistration,
  updateEventRegistration,

  login,
  signup,
  // me
};

module.exports = {
  Mutation,
};
